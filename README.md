# SMTP Server

This will be a small project to test CI features and eventually turn into a CI on SMTP server.

# Goal
The main goal is to forfill the learning goals of the current week of SS3.

## Project structure

This project is to use virtual image to deploy a pre-installed SMTP server in a Debian distribution.

## Description

This project is made to install SMTP server on a default Debian image.

The build.sh will step-by-step:
- Update the repositories
- Install postfix
- Add config choices for automation 

The test.sh will step-by-step:
- Test the postfix config
- Send a dummy email for verification

## How to use
**When running the build.sh you must specify the url**

	git@gitlab.com:ucl-itt-ss3/smtpserver.git
	cd scripts/
	bash ./build.sh
	
## How to test 
**When running the build.sh you must specify the url**

	git@gitlab.com:ucl-itt-ss3/smtpserver.git
    cd scripts/
    bash ./test.sh
    (we have to specify subject, body and receiving email)


## Use with Packer

You are able to install this SMTP server within an packer instance.

What happens is that packer takes a default virtual machine that have been made in example vmware workstation or your preffered hypervisor.  
You just need to get the virtual machine to be stored in one file in the format of "VMDK" and then you edit the config file for packer where it should grab that virtual machine and the output of the finish product with SMTP installed on it.

**Config - Packer.json**

THIS PACKER.JSON CONFIG IS MADE FOR VMWARE - For more support i suggest checking out [Packer WebSite](https://www.packer.io/docs/builders/index.html)

THe smtp.json file is where we configure what packer is going to install inside the virtual machine.

Under the section "builders" you have to change the "$SOURCE" to the location of the virtual machines VMDK from the Hypervisor.

remember to set valid "$OUTPUT"

The section "Provisioners" are all the commands we run inside the virtual machine to get the final product.

~~~~
{
  "builders": [{
    "type": "vmware-vmx",
    "source_path": "$SOURCE",
    "output_directory": "$OUTPUT",
    "communicator": "ssh",
    "ssh_username": "$USER",
    "ssh_password": "$PASSWORD",
    "shutdown_command": "shutdown -h now"
  }],
  
  "Provisioners": [{
    "type": "shell",
    "inline": [
      "apt update -y",
      "apt upgrade -y",
      "apt install git -y",
      "git clone https://gitlab.com/ucl-itt-ss3/smtpserver.git",
      "cd smtpserver",
      "bash ./scripts/build.sh",
      "bash ./scripts/test.sh"
    ]
  }]
}
~~~~

When you want to run the the packer config run the command "packer smtp.json" then sit back and wait we the complete message.



