#!/bin/bash
echo "postfix postfix/mailname string your.hostname.com" | debconf-set-selections
echo "postfix postfix/main_mailer_type string 'Internet Site'" | debconf-set-selections
apt-get update
apt-get install mailutils -y
apt-get install postfix -y